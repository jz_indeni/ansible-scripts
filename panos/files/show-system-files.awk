#!/usr/bin/awk -f
# /var/cores/:
BEGIN {
}

/\/.*\:$/ {
    path = $1
    sub(/:/, "", path)
    if (path !~ /\/$/) {
        path = path "/"
    }
}
# -rw-rw-rw- 1 root root 3.1K Jun  4  2015 dha_6.1.2_0.info
# IMPORTANT: We ignore directories (no "d" in the regex below)
/^[rwx\-]+/ {
    if (path ~ /core/) {
        year=$(NF-1)
        # The "year" column might be a timestamp (like 00:12) which means the year
        # is actually "this" year.
        month=$(NF-3)
        day=$(NF-2)
        file_timestamp = month " " day " " year       
        print path $NF " @@ " file_timestamp 
    }
}


